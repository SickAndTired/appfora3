package com.example.a3testapp.api

import io.reactivex.Single
import javax.inject.Inject
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import java.lang.NullPointerException


class ApiClient @Inject constructor(
    private val dataApi: DataApi,
    private val client: OkHttpClient
) {
    fun getAllUsers() = dataApi.getAllUsers()

    fun getAllAlbumsForUser(userId: Long) = dataApi.getAllAlbumsForUser(userId)

    fun getAllPhotosForAlbum(albumId: Long) = dataApi.getAllPhotosForAlbum(albumId)

    fun downloadImageByUrl(url: String) = Single.create<InputStream> { emitter ->
        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                emitter.onError(e)
            }

            override fun onResponse(call: Call, response: Response) {
                response.body()?.byteStream()?.let {
                    emitter.onSuccess(it)
                } ?: emitter.onError(NullPointerException())
            }
        })
    }!!
}