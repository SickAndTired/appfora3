package com.example.a3testapp.api

import com.example.a3testapp.model.api.NwAlbum
import com.example.a3testapp.model.api.NwPhoto
import com.example.a3testapp.model.api.NwUser
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface DataApi {

    @GET("users")
    fun getAllUsers(): Single<List<NwUser>>

    @GET("albums")
    fun getAllAlbumsForUser(
        @Query("userId") userId: Long
    ): Single<List<NwAlbum>>

    @GET("photos")
    fun getAllPhotosForAlbum(
        @Query("albumId") albumId: Long
    ): Single<List<NwPhoto>>

}