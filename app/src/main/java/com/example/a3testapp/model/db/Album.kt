package com.example.a3testapp.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Album(
    @PrimaryKey
    val id: Long,
    val userId:Long,
    val title: String
)
