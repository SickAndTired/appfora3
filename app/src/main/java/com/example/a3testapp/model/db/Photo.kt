package com.example.a3testapp.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Photo(
    @PrimaryKey
    val id: Long,
    val albumId: Long,
    val title: String,
    val url: String
)