package com.example.a3testapp.model.api

data class NwPhoto(
    val id: Long,
    val albumId: Long,
    val title: String,
    val url: String
)
