package com.example.a3testapp.model.api

data class NwAlbum(
    val id: Long,
    val title: String,
    val userId: Long
)