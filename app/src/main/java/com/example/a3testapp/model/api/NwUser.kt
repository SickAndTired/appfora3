package com.example.a3testapp.model.api

data class NwUser(
    val id: Long,
    val name: String
)
