package com.example.a3testapp.mvp

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.a3testapp.Constants
import com.example.a3testapp.R
import com.example.a3testapp.api.ApiClient
import com.example.a3testapp.db.DataBase
import com.example.a3testapp.model.db.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject

@InjectViewState
class UsersPresenter : MvpPresenter<UsersView>() {

    @Inject
    lateinit var apiClient: ApiClient

    @Inject
    lateinit var appDatabase: DataBase

    @Inject
    lateinit var router: Router

    private var compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Toothpick.inject(this, Toothpick.openScope(Constants.APP_SCOPE))
        getAllNwUsers()
        showAllUsers()
    }

    fun getAllNwUsers() {
        compositeDisposable.add(
            apiClient.getAllUsers()
                .map { listNwUsers ->
                    listNwUsers.map {
                        User(
                            id = it.id,
                            name = it.name
                        )
                    }
                }
                .doOnSuccess { appDatabase.userDao().insert(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showSwipeProgressBar(true) }
                .doOnEvent { _, _ -> viewState.showSwipeProgressBar(false) }
                .subscribeBy(
                    onSuccess = { },
                    onError = {
                        viewState.showMessage(R.string.error_no_connection)
                        Timber.e(it)
                    }
                )
        )
    }

    private fun showAllUsers() {
        compositeDisposable.add(appDatabase.userDao().getAllFlowable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { viewState.showData(it) },
                onError = { Timber.e(it) }
            ))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun onUserClicked(userId: Long) {
        router.navigateTo(Constants.ALBUMS_SCREEN, userId)
    }
}