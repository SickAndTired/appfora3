package com.example.a3testapp.mvp

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.a3testapp.Constants
import com.example.a3testapp.R
import com.example.a3testapp.api.ApiClient
import com.example.a3testapp.db.DataBase
import com.example.a3testapp.model.db.Photo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.Toothpick
import java.io.File
import java.io.InputStream
import javax.inject.Inject
import kotlin.properties.Delegates


@InjectViewState
class PhotosPresenter : MvpPresenter<PhotosView>() {

    var albumId: Long by Delegates.notNull()

    @Inject
    lateinit var apiClient: ApiClient

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var appDatabase: DataBase

    @Inject
    lateinit var router: Router

    private val downloadingUrls = mutableListOf<String>()

    private var compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Toothpick.inject(this, Toothpick.openScope(Constants.APP_SCOPE))
        getPhotosForThisAlbum()
        showAllPhotos()
    }

    fun getPhotosForThisAlbum() {
        compositeDisposable.add(
            apiClient.getAllPhotosForAlbum(albumId)
                .map { nwPhotoList ->
                    nwPhotoList.map {
                        Photo(
                            id = it.id,
                            title = it.title,
                            albumId = it.albumId,
                            url = it.url
                        )
                    }
                }
                .doOnSuccess { appDatabase.photoDao().insert(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showSwipeProgressBar(true) }
                .doOnEvent { _, _ -> viewState.showSwipeProgressBar(false) }
                .subscribeBy(
                    onSuccess = { },
                    onError = {
                        viewState.showMessage(R.string.error_no_connection)
                        Timber.e(it)
                    }
                )
        )
    }

    private fun File.copyInputStreamToFile(inputStream: InputStream) {
        inputStream.use { input ->
            this.outputStream().use { fileOut ->
                input.copyTo(fileOut)
            }
        }
    }

    private fun showAllPhotos() {
        compositeDisposable.add(appDatabase.photoDao().getAllByAlbumId(albumId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { viewState.showData(it) },
                onError = { Timber.e(it) }
            ))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    private fun downloadAndSaveImage(photo: Photo) =
        apiClient.downloadImageByUrl(photo.url)
            .map { Pair(photo, it) }
            .doOnSuccess { photoAndOneImage ->
                val filePath = context.filesDir.absoluteFile
                val filename = "${photoAndOneImage.first.id}.png"
                Timber.i("Local filename:%s", filename)
                val file = File(filePath, filename)
                if (file.createNewFile()) {
                    file.createNewFile()
                }
                file.copyInputStreamToFile(photoAndOneImage.second)
            }

    fun downloadImage(position: Int, photo: Photo) {
        if (!downloadingUrls.contains(photo.url)) {
            compositeDisposable.add(downloadAndSaveImage(photo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { downloadingUrls += photo.url }
                .doOnEvent { _, _ ->
                    downloadingUrls -= photo.url
                    viewState.notifyByPosition(position)
                }
                .subscribeBy(
                    onSuccess = { },
                    onError = { Timber.e(it) }
                ))
        }
    }

    fun onBackClicked() {
        router.exit()
    }
}