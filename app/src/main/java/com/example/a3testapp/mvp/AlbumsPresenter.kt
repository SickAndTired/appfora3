package com.example.a3testapp.mvp

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.a3testapp.Constants
import com.example.a3testapp.R
import com.example.a3testapp.api.ApiClient
import com.example.a3testapp.db.DataBase
import com.example.a3testapp.model.db.Album
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject
import kotlin.properties.Delegates

@InjectViewState
class AlbumsPresenter : MvpPresenter<AlbumsView>() {

    var userId: Long by Delegates.notNull()

    @Inject
    lateinit var apiClient: ApiClient

    @Inject
    lateinit var appDatabase: DataBase

    @Inject
    lateinit var router: Router

    private var compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        Toothpick.inject(this, Toothpick.openScope(Constants.APP_SCOPE))
        getNwAlbumsForThisUser()
        showAllAlbums()
    }

    fun getNwAlbumsForThisUser() {
        compositeDisposable.add(
            apiClient.getAllAlbumsForUser(userId)
                .map { listNwAlbums ->
                    listNwAlbums.map {
                        Album(
                            title = it.title,
                            userId = it.userId,
                            id = it.id
                        )
                    }
                }
                .doOnSuccess { appDatabase.albumDao().insert(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showSwipeProgressBar(true) }
                .doOnEvent { _, _ -> viewState.showSwipeProgressBar(false) }
                .subscribeBy(
                    onSuccess = { },
                    onError = {
                        viewState.showMessage(R.string.error_no_connection)
                        Timber.e(it)
                    }
                )
        )
    }

    private fun showAllAlbums() {
        compositeDisposable.add(appDatabase.albumDao().getAllByUserId(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { viewState.showData(it) },
                onError = { Timber.e(it) }
            ))
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun onAlbumClicked(albumId: Long) {
        router.navigateTo(Constants.PHOTOS_SCREEN, albumId)
    }

    fun onBackClicked() {
        router.exit()
    }
}