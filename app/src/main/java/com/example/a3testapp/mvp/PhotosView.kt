package com.example.a3testapp.mvp

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.a3testapp.model.db.Photo

interface PhotosView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(@StringRes message: Int)

    fun showData(listPhoto: List<Photo>)

    fun notifyByPosition(position: Int)

    fun showSwipeProgressBar(showSwipeProgressBar: Boolean)
}