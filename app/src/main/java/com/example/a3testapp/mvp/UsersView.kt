package com.example.a3testapp.mvp

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.a3testapp.model.db.User

interface UsersView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(@StringRes message: Int)

    fun showData(listUser: List<User>)

    fun showSwipeProgressBar(showSwipeProgressBar:Boolean)
}