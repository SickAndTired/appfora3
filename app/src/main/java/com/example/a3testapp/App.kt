package com.example.a3testapp

import android.app.Application
import com.example.a3testapp.di.AppModule
import timber.log.Timber
import toothpick.Toothpick

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Toothpick.openScope(Constants.APP_SCOPE).installModules(AppModule(this))
    }
}