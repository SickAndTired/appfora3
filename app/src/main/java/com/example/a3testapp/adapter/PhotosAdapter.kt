package com.example.a3testapp.adapter

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a3testapp.R
import com.example.a3testapp.model.db.Photo
import kotlinx.android.synthetic.main.item_photo.view.*
import timber.log.Timber
import java.io.File

class PhotosAdapter(private val photoList: List<Photo>, private val downloadHandler: (Int, Photo) -> Unit) :
    RecyclerView.Adapter<PhotosAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false))
    }

    override fun onBindViewHolder(holder: PhotosAdapter.ViewHolder, position: Int) {
        holder.bindItems(photoList[position])
    }

    override fun getItemCount(): Int {
        return photoList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(photo: Photo) {
            itemView.tvPhotoName.text = photo.title
            val filePath = itemView.context.filesDir.absoluteFile
            val filename = "${photo.id}.png"
            Timber.i("Local filename:%s", filename)
            val file = File(filePath, filename)
            if (file.exists()) {
                val myBitmap = BitmapFactory.decodeFile(file.absolutePath)
                itemView.ivPhoto.setImageBitmap(myBitmap)
                itemView.progressBar.visibility = View.GONE
            } else {
                downloadHandler.invoke(adapterPosition, photo)
                itemView.progressBar.visibility = View.VISIBLE
                itemView.ivPhoto.setImageBitmap(null)
            }
        }
    }
}