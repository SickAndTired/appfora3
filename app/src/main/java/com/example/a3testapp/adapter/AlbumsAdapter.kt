package com.example.a3testapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a3testapp.R
import com.example.a3testapp.model.db.Album
import kotlinx.android.synthetic.main.item_album.view.*

class AlbumsAdapter(private val albumList: List<Album>, private val albumClickListener: (Long) -> Unit) :
    RecyclerView.Adapter<AlbumsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false))
    }

    override fun onBindViewHolder(holder: AlbumsAdapter.ViewHolder, position: Int) {
        holder.bindItems(albumList[position])
    }

    override fun getItemCount(): Int {
        return albumList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(album: Album) {
            itemView.tvAlbumName.text = album.title
            itemView.setOnClickListener { albumClickListener.invoke(album.id) }
        }
    }
}