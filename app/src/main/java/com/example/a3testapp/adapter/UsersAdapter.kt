package com.example.a3testapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a3testapp.R
import com.example.a3testapp.model.db.User
import kotlinx.android.synthetic.main.item_user.view.*


class UsersAdapter(private val userList: List<User>, private val userClickListener: (Long) -> Unit) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))
    }

    override fun onBindViewHolder(holder: UsersAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: User) {
            itemView.tvUserName.text = user.name
            itemView.setOnClickListener { userClickListener.invoke(user.id) }
        }
    }
}