package com.example.a3testapp

object Constants {

    const val APP_SCOPE = "APP_SCOPE"
    const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    const val USERS_SCREEN = "USERS_SCREEN"
    const val ALBUMS_SCREEN = "ALBUMS_SCREEN"
    const val PHOTOS_SCREEN = "PHOTOS_SCREEN"
}
