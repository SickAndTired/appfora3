package com.example.a3testapp.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.a3testapp.model.db.Album
import io.reactivex.Flowable

@Dao
interface AlbumDao {

    @Query("SELECT * FROM Album WHERE userId = :userId")
    fun getAllByUserId(userId: Long): Flowable<List<Album>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(listAlbum: List<Album>): List<Long>
}