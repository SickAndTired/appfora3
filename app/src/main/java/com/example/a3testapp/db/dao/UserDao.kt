package com.example.a3testapp.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.a3testapp.model.db.User
import io.reactivex.Flowable

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getAllFlowable(): Flowable<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(listUser: List<User>): List<Long>
}