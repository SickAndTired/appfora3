package com.example.a3testapp.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.a3testapp.model.db.Photo
import io.reactivex.Flowable

@Dao
interface PhotoDao {

    @Query("SELECT * FROM Photo WHERE albumId = :albumId")
    fun getAllByAlbumId(albumId: Long): Flowable<List<Photo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(listPhoto: List<Photo>): List<Long>
}