package com.example.a3testapp.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.a3testapp.db.dao.AlbumDao
import com.example.a3testapp.db.dao.PhotoDao
import com.example.a3testapp.db.dao.UserDao
import com.example.a3testapp.model.db.Album
import com.example.a3testapp.model.db.Photo
import com.example.a3testapp.model.db.User

@Database(
    entities = [
        User::class,
        Album::class,
        Photo::class
    ],
    version = 1
)

abstract class DataBase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun albumDao(): AlbumDao

    abstract fun photoDao(): PhotoDao
}