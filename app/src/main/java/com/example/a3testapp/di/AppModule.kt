package com.example.a3testapp.di

import android.arch.persistence.room.Room
import android.content.Context
import com.example.a3testapp.Constants
import com.example.a3testapp.api.ApiClient
import com.example.a3testapp.api.DataApi
import com.example.a3testapp.db.DataBase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.config.Module

class AppModule(context: Context) : Module() {

    init {
        bind(Context::class.java).toInstance(context)

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor { log -> Timber.d(log) }.setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()

        bind(OkHttpClient::class.java).toInstance(okHttpClient)

        val dataRetrofit =
            Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        val dataApi = dataRetrofit.create(DataApi::class.java)

        bind(DataApi::class.java).toInstance(dataApi)

        bind(ApiClient::class.java).singletonInScope()

        val cicerone = Cicerone.create()
        bind(Cicerone::class.java).toInstance(cicerone)
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        bind(DataBase::class.java).toInstance(
            Room.databaseBuilder(
                context,
                DataBase::class.java,
                "database"
            )
                .build()
        )
    }
}