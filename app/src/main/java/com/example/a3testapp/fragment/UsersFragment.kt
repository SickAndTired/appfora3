package com.example.a3testapp.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.a3testapp.R
import com.example.a3testapp.adapter.UsersAdapter
import com.example.a3testapp.model.db.User
import com.example.a3testapp.mvp.UsersPresenter
import com.example.a3testapp.mvp.UsersView
import kotlinx.android.synthetic.main.fragment_users.*

class UsersFragment : MvpAppCompatFragment(), UsersView {
    @InjectPresenter
    lateinit var usersPresenter: UsersPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usersRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
        swipeRefreshLayout.setOnRefreshListener { usersPresenter.getAllNwUsers() }
    }

    override fun showSwipeProgressBar(showSwipeProgressBar: Boolean) {
        swipeRefreshLayout.isRefreshing = showSwipeProgressBar
    }

    override fun showData(listUser: List<User>) {
        usersRecyclerView.adapter = UsersAdapter(listUser) { usersPresenter.onUserClicked(it) }
    }

    override fun showMessage(message: String) {
        if (isAdded) {
            activity?.let { Toast.makeText(it, message, Toast.LENGTH_LONG).show() }
        }
    }

    override fun showMessage(message: Int) {
        if (isAdded) {
            showMessage(getString(message))
        }
    }

    companion object {
        fun newInstance() = UsersFragment()
    }
}
