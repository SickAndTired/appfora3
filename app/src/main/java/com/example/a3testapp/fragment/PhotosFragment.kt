package com.example.a3testapp.fragment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.example.a3testapp.R
import com.example.a3testapp.adapter.PhotosAdapter
import com.example.a3testapp.model.db.Photo
import com.example.a3testapp.mvp.PhotosPresenter
import com.example.a3testapp.mvp.PhotosView
import kotlinx.android.synthetic.main.fragment_photos.*
import timber.log.Timber

class PhotosFragment : MvpAppCompatFragment(), PhotosView {
    @InjectPresenter
    lateinit var photosPresenter: PhotosPresenter

    @ProvidePresenter
    fun providePresenter(): PhotosPresenter {
        Timber.d("providePresenter: ${arguments?.getLong(PhotosFragment.ARG_ALBUM_ID)}")
        val presenter = PhotosPresenter()
        presenter.albumId = arguments?.getLong(PhotosFragment.ARG_ALBUM_ID)
            ?: throw IllegalStateException("cant create presenter without albumId in fragment args!")
        return presenter
    }

    @ProvidePresenterTag(presenterClass = PhotosPresenter::class)
    fun providePresenterTag(): String = arguments?.getLong(PhotosFragment.ARG_ALBUM_ID)?.toString()
        ?: throw IllegalStateException("cant create presenter without albumId in fragment args!")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
                it.setHomeButtonEnabled(true)
            }
        }
        toolbar.setNavigationOnClickListener { photosPresenter.onBackClicked() }
        photosRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
        swipeRefreshLayout.setOnRefreshListener { photosPresenter.getPhotosForThisAlbum() }

    }

    override fun showSwipeProgressBar(showSwipeProgressBar: Boolean) {
        swipeRefreshLayout.isRefreshing = showSwipeProgressBar
    }

    override fun showData(listPhoto: List<Photo>) {
        photosRecyclerView.adapter =
            PhotosAdapter(listPhoto) { position, photo -> photosPresenter.downloadImage(position, photo) }
    }

    override fun notifyByPosition(position: Int) {
        photosRecyclerView.adapter?.notifyItemChanged(position)
    }

    override fun showMessage(message: String) {
        if (isAdded) {
            activity?.let { Toast.makeText(it, message, Toast.LENGTH_LONG).show() }
        }
    }

    override fun showMessage(message: Int) {
        if (isAdded) {
            showMessage(getString(message))
        }
    }

    companion object {
        private const val ARG_ALBUM_ID = "ARG_ALBUM_ID"

        fun newInstance(albumId: Long): PhotosFragment {
            val fragment = PhotosFragment()
            val args = Bundle()
            args.putLong(ARG_ALBUM_ID, albumId)
            fragment.arguments = args
            return fragment
        }
    }
}
