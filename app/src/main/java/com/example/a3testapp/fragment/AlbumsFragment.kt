package com.example.a3testapp.fragment

import android.Manifest
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.arellomobile.mvp.presenter.ProvidePresenterTag
import com.example.a3testapp.R
import com.example.a3testapp.adapter.AlbumsAdapter
import com.example.a3testapp.model.db.Album
import com.example.a3testapp.mvp.AlbumsPresenter
import com.example.a3testapp.mvp.AlbumsView
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_albums.*
import timber.log.Timber

class AlbumsFragment : MvpAppCompatFragment(), AlbumsView {

    @InjectPresenter
    lateinit var albumsPresenter: AlbumsPresenter

    @ProvidePresenter
    fun providePresenter(): AlbumsPresenter {
        Timber.d("providePresenter: ${arguments?.getLong(ARG_USER_ID)}")
        val presenter = AlbumsPresenter()
        presenter.userId = arguments?.getLong(ARG_USER_ID)
            ?: throw IllegalStateException("cant create presenter without userId in fragment args!")
        return presenter
    }

    @ProvidePresenterTag(presenterClass = AlbumsPresenter::class)
    fun providePresenterTag(): String = arguments?.getLong(ARG_USER_ID)?.toString()
        ?: throw IllegalStateException("cant create presenter without userId in fragment args!")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_albums, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
                it.setHomeButtonEnabled(true)
            }
        }

        toolbar.setNavigationOnClickListener { albumsPresenter.onBackClicked() }
        albumsRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
        swipeRefreshLayout.setOnRefreshListener { albumsPresenter.getNwAlbumsForThisUser() }
    }

    override fun showSwipeProgressBar(showSwipeProgressBar: Boolean) {
        swipeRefreshLayout.isRefreshing = showSwipeProgressBar
    }

    override fun showData(listAlbum: List<Album>) {
        albumsRecyclerView.adapter = AlbumsAdapter(listAlbum) {
            RxPermissions(this)
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribeBy { granted ->
                    if (granted) {
                        albumsPresenter.onAlbumClicked(it)
                    } else {
                        showMessage(R.string.write_permission_alert)
                    }
                }
        }
    }

    override fun showMessage(@StringRes message: Int) {
        if (isAdded) {
            showMessage(getString(message))
        }
    }

    override fun showMessage(message: String) {
        if (isAdded) {
            activity?.let { Toast.makeText(it, message, Toast.LENGTH_LONG).show() }
        }
    }

    companion object {
        private const val ARG_USER_ID = "ARG_USER_ID"

        fun newInstance(userId: Long): AlbumsFragment {
            val fragment = AlbumsFragment()
            val args = Bundle()
            args.putLong(ARG_USER_ID, userId)
            fragment.arguments = args
            return fragment
        }
    }
}