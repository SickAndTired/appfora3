package com.example.a3testapp.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.a3testapp.Constants
import com.example.a3testapp.R
import com.example.a3testapp.fragment.AlbumsFragment
import com.example.a3testapp.fragment.PhotosFragment
import com.example.a3testapp.fragment.UsersFragment
import com.example.a3testapp.mvp.MainPresenter
import com.example.a3testapp.mvp.MainView
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), MainView {

    @Suppress("unused")
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter


    private val navigator: Navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.container) {

        override fun createFragment(screenKey: String, data: Any?): Fragment =
            when (screenKey) {
                Constants.USERS_SCREEN -> UsersFragment.newInstance()
                Constants.ALBUMS_SCREEN -> AlbumsFragment.newInstance(data as Long)
                Constants.PHOTOS_SCREEN -> PhotosFragment.newInstance(data as Long)
                else -> throw RuntimeException("Unknown screen key !")
            }

        override fun showSystemMessage(message: String?) {
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        }

        override fun exit() {
            back()
        }
    }

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Toothpick.inject(this, Toothpick.openScope(Constants.APP_SCOPE))

    }


    override fun onResumeFragments() {
        super.onResumeFragments()
        Timber.d("navigator holder :%s", navigatorHolder)
        Timber.d("navigator :%s", navigator)
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun showMessage(message: Int) {
        showMessage(getString(message))
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
